''' Ejemplo TDA 1: Funcionalidad ESPECIAL de VECTORES (ARRAYS)

    Operaciones: 
    
        [HECHO] 1) Crear
        [HECHO] 2) Llenar
        [HECHO] 3) Obtener posicion inicio
        [HECHO] 4) Obtener posicion fin
        [HECHO] 5) Obtener cantidad elementos    
        [HECHO] 6) Mostrar todo los elementos
        [HECHO] 7) Mostrar elemento del inicio
        [HECHO] 8) Mostrar elemento del fin
        9) Aumentar tamano del arreglo
        10) Disminuir tamano del arreglo    
        11) Insertar elemento en posicion especifica
        12) Eliminar elemento de posicion especifica
        13) Invertir el vector

'''
import random

class VectorEspecial:
    '''Clase para crear Vectores especiales'''
    
    #1) crear
    def __init__ (self,tamano):
        self.numeros = []
        self.tamano = tamano
        
    #2) llenar    
    def llenarVectorAutomatizado(self):
        for i in range(self.tamano):
            self.numeros.append(int (random.randrange(1,100,1)))
    
    def llenarVectorTeclado(self):
        print("ingresa numeros para llenar el vector")
        for i in range(self.tamano):
            n = input("ingresa calificacion" + (i+1) + ": ")
            self.numeros.append(int (n))
    
    #6) Mostrar todo los elementos        
    def mostrarVector(self):
        for i in range(len(self.numeros)):
            print(self.numeros[i], end=" ")
        print()
    
    #3) Obtener posicion inicio
    def obtenerPosicionInicio(self):
        print(0)        
            
    #4) Obtener posicion fin
    def obtenerPosicionFin(self):
        print(len(self.numeros)-1) 
         
    #5) Obtener cantidad elementos            
    def obtenerCantidadElementos(self):
        print(len(self.numeros)) 
        
    #7) Mostrar elemento del inicio
    def mostrarElementoInicio(self):
        print(self.numeros[0]) 
    
    #8) Mostrar elemento del fin
    def mostrarElementoFin(self):
        print(self.numeros[len(self.numeros)-1]) 
    
    #9) Aumentar tamano del arreglo
    def aumentarTamanoArreglo(self):
        elec = int(input("Ingrese cuanto lo quiere aumentar: "))
        temNum = self.numeros
        for i in range(elec):
            temNum.append(0)
            
        self.numeros = temNum
        print(self.numeros)
        
    #10) Disminuir tamano del arreglo    
    def disminuirTamanoArreglo(self):
        elec = int(input("Ingrese cuanto lo quiere disminuir: "))
        temNum = []
        for i in range(len(self.numeros)-elec):
            temNum.append(self.numeros[i])
            
        self.numeros = temNum
        print(self.numeros)
        
    #11) Insertar elemento en posicion especifica
    def insertarElementoEspecifico(self):
        elec = int(input("Ingrese la posicion a ingresar: "))
        self.numeros[elec-1] = int(input("Ingrese el Numero a Ingresar: "))
        print(self.numeros)
        
    #12) Eliminar elemento de posicion especifica
    def eliminarElementoEspecifico(self):
        elec = int(input("Ingrese la posicion a eliminar: "))
        self.numeros[elec-1] = 0
        print(self.numeros)
        
    #13) Invertir el vector
    def invertirVector(self):
        temNum = []
        for i in range(len(self.numeros)):
            temNum.append(self.numeros[len(self.numeros)-i-1])
            
        self.numeros = temNum
        print(self.numeros)
        
        
            
                
ve1 = VectorEspecial(10)
ve1.llenarVectorAutomatizado();
print(ve1.numeros)

opcion = 0
while opcion != "12":
    print("--------------------------------------------------------------")
    print("1.Obtener posici�n inicio")
    print("2.Obtener posici�n fin")
    print("3.Obtener cantidad elementos")
    print("4.Mostrar todo los elementos")
    print("5.Mostrar elemento del inicio")
    print("6.Mostrar elemento del fin")
    print("7.Aumentar tama�o del arreglo")
    print("8.Disminuir tama�o del arreglo ")
    print("9.Insertar elemento en posici�n especifica")
    print("10.Eliminar elemento de posici�n especifica")
    print("11.Invertir el vector")
    print("12.Salir")
    print("------------------------------")
    print()
    opcion = input("Opcion: ")
    
    if opcion == "1":
        ve1.obtenerPosicionInicio()
    elif opcion == "2":
        ve1.obtenerPosicionFin()
    elif opcion == "3":
        ve1.obtenerCantidadElementos()
    elif opcion == "4":
        ve1.mostrarVector()
    elif opcion == "5":
        ve1.mostrarElementoInicio()
    elif opcion == "6":
        ve1.mostrarElementoFin()
    elif opcion == "7":
        ve1.aumentarTamanoArreglo()
    elif opcion == "8":
        ve1.disminuirTamanoArreglo()
    elif opcion == "9":
        ve1.insertarElementoEspecifico()
    elif opcion == "10":
        ve1.eliminarElementoEspecifico()
    elif opcion == "11":
        ve1.invertirVector()
    elif opcion == "12":
        print("BYE BYE :3")
    else:
        print("ESA NO ES UNA OPCION VALIDA")
            
            
            
            
            
            
            
            
            
            
            